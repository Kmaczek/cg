﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Brass.Logic.Boards;
using Brass.Logic.Players;
using Brass.Logic.Spells;
using Brass.Logic.Units;
using log4net;

namespace Brass
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MainWindow));

        public MainWindow()
        {
            log4net.Config.BasicConfigurator.Configure();

            InitializeComponent();  

            //var spell = new SpellBase(1);
            //var unit = new UnitBase(1);
            //var player = new Player("aaa");
            //player.SpellList.Add(spell);
            var board = new Board();
            
            //Log.Info(spell.ToString());
            
            Log.Info("this is the first log message");
            Log.Warn("nooooooooooooooooooooooooo");
        }
    }
}
