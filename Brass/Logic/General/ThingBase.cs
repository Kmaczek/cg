﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Brass.Logic.Constants;

namespace Brass.Logic.General
{
    /// <summary>
    /// Class that is base to Player, Spell, Ability, Item, Unit. In general
    /// to almost anything that makes interaction beetwen thamselves
    /// </summary>
    public abstract class ThingBase
    {
            #region Constructors

        protected ThingBase()
        {
            Name = "None";
            TargetType = ThingType.None;
        }

        protected ThingBase(String name, ThingType type)
        {
            Name = name;
            TargetType = type;
        }

        protected ThingBase(String name)
        {
            Name = name;
            TargetType = ThingType.None;
        }

        protected ThingBase(ThingType type)
        {
            Name = "None";
            TargetType = type;
        }

            #endregion

            #region Methods

        public override string ToString()
        {
            return String.Format("Thing of Name: {0} and Type: {1}", Name, TargetType);
        }

            #endregion

            #region Properties

        public String Name { get; protected set; }
        public ThingType TargetType { get; protected set; }

            #endregion
    }

}
