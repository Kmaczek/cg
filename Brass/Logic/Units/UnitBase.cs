﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Brass.Logic.Constants;
using Brass.Logic.General;
using Brass.Logic.Helpers;
using Brass.Logic.Spells;

namespace Brass.Logic.Units
{
    public class UnitBase : ThingBase
    {

            #region Constructors

        public UnitBase(): base("UnitBase", ThingType.Unit)
        {

        }

        public UnitBase(short id) : base(ThingType.Unit)
        {
            Id = id;

            InitFields();
            ParseData();
        }

            #endregion
        
            #region Properties

        public short Id { get; private set; }
        public School SchoolPrimary { get; private set; }
        public School SchoolSecondary { get; private set; }
        public UnitAttributes Attributes { get; set; }
        public List<Target> Targets { get; set; }
        public UnitType Types { get; set; }
        public UnitState States { get; set; }
         
            #endregion

            #region Methods

        /// <summary>
        /// Used in constructor to initialize fields
        /// </summary>
        private void InitFields()
        {
            Attributes = new UnitAttributes();
        }

        private void ParseData()
        {
            if (Id < 1)
            {
                throw new ArgumentException("Incorrect value of Id: " + Id);
            }

            var stuff = _xElement.Elements().ToList();
            var result = (from s in stuff
                          where (short)s.Element(XUnitBase.Id) == Id
                          select s).FirstOrDefault();

            if (result == null)
            {
                throw new ArgumentNullException("Be sure that field _xElement " +
                                                "is set properly");
            }

            Name = (string)result.Element(XUnitBase.Name);
            var help = new XmlHelper();
            Attributes = help.ParseUnitAtributes(result.Element(XUnitBase.Attributes));
            SchoolPrimary = help.ParseSchool(result.Element(XUnitBase.SchoolPrimary));
            SchoolSecondary = help.ParseSchool(result.Element(XUnitBase.SchoolSecondary));
            Targets = help.ParseTarget(result.Element(XUnitBase.Targets));
            Types = help.ParseUnitType(result.Element(XUnitBase.Types));
            States = help.ParseUnitState(result.Element(XUnitBase.States));
        }

            #endregion

            #region Private Fields

        readonly XElement _xElement = XElement.Parse(Properties.Resources.UnitTesting);

            #endregion

    }

    public static class XUnitBase
    {
        public static String Id = "Id";
        public static String Name = "Name";
        public static String SchoolPrimary = "SchoolPrimary";
        public static String SchoolSecondary = "SchoolSecondary";
        public static String Attributes = "Attributes";
        public static String Targets = "Targets";
        public static String Types = "Types";
        public static String States = "States";

        //Unit atributes
        public static String Health = "Health";
        public static String Magic = "Magic";
        public static String Might = "Might";
        public static String Resistance = "Resistance";
        public static String Quickness = "Quickness";
        public static String GoldCost = "GoldCost";
        public static String GoldCostCurrent = "GoldCostCurrent";
        public static String SellCost = "SellCost";
        public static String SellCostCurrent = "SellCostCurrent";
    }
}   
