﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Xml.Linq;
using Brass.Logic.Constants;
using Brass.Logic.Spells;
using Brass.Logic.Units;

namespace Brass.Logic.Helpers //TODO testy
{
    public class XmlHelper
    {
            #region spell specific

        /// <summary>
        /// Parsing Attributes from XElement
        /// </summary>
        /// <param name="attributes">Pass result.Element(XSpellBase.Attributes) into it</param>
        /// <exception cref="ArgumentNullException">When attributes is null</exception>
        /// <returns></returns>
        public SpellAttributes ParseSpellAtributes(XElement attributes)
        {
            if (attributes == null)
            {
                throw new ArgumentNullException("attributes");
            }
            var result = new SpellAttributes();

            result.BuyPrice = (short)attributes.Element(XSpellBase.BuyPrice);
            result.BuyPriceCurrent = result.BuyPrice;

            result.Cooldown = (short)attributes.Element(XSpellBase.Cooldown);
            result.CooldownCurrent = result.Cooldown;

            result.GoldCost = (short)attributes.Element(XSpellBase.GoldCost);
            result.GoldCostCurrent = result.GoldCost;

            result.ManaCost = (short)attributes.Element(XSpellBase.ManaCost);
            result.ManaCostCurrent = result.ManaCost;

            result.SellPrice = (short)attributes.Element(XSpellBase.SellPrice);
            result.SellPriceCurrent = result.SellPrice;

            return result;
        }

        /// <summary>
        /// Parsing SpellState from XEmelent
        /// </summary>
        /// <param name="states">Pass value from Element(XSpellBase.States)</param>
        /// <exception cref="ArgumentNullException">When states is null</exception>
        /// <returns></returns>
        public SpellState ParseSpellState(XElement states)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            var result = SpellState.None;

            var st = states.Elements().ToList();
            foreach (var s in st)
            {
                SpellState state;
                Enum.TryParse(s.Value, true, out state);
                result = result | state;
            }

            return result;
        }

        /// <summary>
        /// Parsing Target from XEmelent
        /// </summary>
        /// <param name="types">Pass value from Element(XSpellBase.Types)</param>
        /// <exception cref="ArgumentNullException">When target is null</exception>
        /// <returns></returns>
        public SpellType ParseSpellType(XElement types)
        {
            if (types == null)
            {
                throw new ArgumentNullException("types");
            }
            var result = SpellType.None;

            var flags = types.Elements().ToList();
            foreach (var s in flags)
            {
                SpellType type;
                Enum.TryParse(s.Value, true, out type);
                result = result | type;
            }

            return result;
        }

            #region HealthSpell
        
        

            #endregion

            #endregion

            #region unit specific

        /// <summary>
        /// Parsing UnitAttributes from XElement
        /// </summary>
        /// <param name="attributes">Pass result.Element(XUnitBase.Attributes) into it</param>
        /// <exception cref="ArgumentNullException">When attributes is null</exception>
        /// <returns></returns>
        public UnitAttributes ParseUnitAtributes(XElement attributes)
        {
            if (attributes == null)
            {
                throw new ArgumentNullException("attributes");
            }
            var result = new UnitAttributes();

            result.Health = (short)attributes.Element(XUnitBase.Health);
            result.Magic = (short)attributes.Element(XUnitBase.Magic);
            result.Might = (short)attributes.Element(XUnitBase.Might);
            result.Quickness = (short)attributes.Element(XUnitBase.Quickness);
            result.Resistance = (short)attributes.Element(XUnitBase.Resistance);

            result.GoldCost = (short)attributes.Element(XUnitBase.GoldCost);
            result.GoldCostCurrent = result.GoldCost;
            result.SellCost = (short)attributes.Element(XUnitBase.SellCost);
            result.SellCostCurrent = result.SellCost;

            return result;
        }

        /// <summary>
        /// Parsing Unit Type from XEmelent
        /// </summary>
        /// <param name="types">Pass value from Element(XSpellBase.Types)</param>
        /// <exception cref="ArgumentNullException">When target is null</exception>
        /// <returns></returns>
        public UnitType ParseUnitType(XElement types)
        {
            if (types == null)
            {
                throw new ArgumentNullException("types");
            }
            var result = UnitType.None;

            var flags = types.Elements().ToList();
            foreach (var s in flags)
            {
                UnitType type;
                Enum.TryParse(s.Value, true, out type);
                result = result | type;
            }

            return result;
        }

        /// <summary>
        /// Parsing UnitState from XEmelent
        /// </summary>
        /// <param name="states">Pass value from Element(XUnitBase.States)</param>
        /// <exception cref="ArgumentNullException">When states is null</exception>
        /// <returns></returns>
        public UnitState ParseUnitState(XElement states)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            var result = UnitState.None;

            var st = states.Elements().ToList();
            foreach (var s in st)
            {
                UnitState state;
                Enum.TryParse(s.Value, true, out state);
                result = result | state;
            }

            return result;
        }

            #endregion

            #region general

        /// <summary>
        /// Parsing Target from XEmelent
        /// </summary>
        /// <param name="targets">Pass value from Element(XSpellBase.Targets)</param>
        /// <exception cref="ArgumentNullException">When target is null</exception>
        /// <returns>Returns list with Target structs of all targets</returns>
        public List<Target> ParseTarget(XElement targets)
        {
            if (targets == null)
            {
                throw new ArgumentNullException("targets");
            }
            var result = new List<Target>();

            var flags = targets.Elements().ToList();
            foreach (var s in flags)
            {
                var target = new Target();
                var xElement = s.Element(XTarget.Affiliation);
                if (xElement != null)
                    Enum.TryParse(xElement.Value, true, out target.Affiliation);
                var element = s.Element(XTarget.Attitude);
                if (element != null)
                    Enum.TryParse(element.Value, true, out target.Attitude);
                result.Add(target);
            }

            return result;
        }

        /// <summary>
        /// Parsing School from XEmelent
        /// </summary>
        /// <param name="school">Pass value from Element(XSpellBase.Primary or Secondary)</param>
        /// <exception cref="ArgumentNullException">When school is null</exception>
        /// <returns></returns>
        public School ParseSchool(XElement school)
        {
            if (school == null)
            {
                throw new ArgumentNullException("school");
            }

            School result;
            Enum.TryParse(school.Value, true, out result);

            return result;
        }

        /// <summary>
        /// Helps with extracting numbers of short type from xml
        /// </summary>
        /// <param name="numberXml">Pass XElement, be sure that it contains unsigned int number, 
        /// otherwise Parse will throw exeption</param>
        /// <returns>Short number from given element</returns>
        public short ParseNumberShort(XElement numberXml)
        {
            if (numberXml == null)
            {
                throw new ArgumentNullException("numberXml");
            }

            return short.Parse(numberXml.Value);
        }

            #endregion
       
        //Make struct containing UnitState, SpellState, PlayerState and 
        //instead of 3 methods make one that returns this struct
    }
}
