﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Brass.Logic.Constants;
using Brass.Logic.General;
using Brass.Logic.Spells;
using Brass.Logic.Units;

namespace Brass.Logic.Players
{
    public class Player : ThingBase
    {
        #region Constructors

        public Player()
            : base(ThingType.Player)
        {
            InitFields();
        }

        public Player(String name)
            : base("", ThingType.Player)
        {
            Name = name;
            InitFields();
            
        }

        #endregion

        #region Properties

        public PlayerAttributes Attributes { get; set; }
        public PlayerState States { get; set; }
        public List<UnitBase> UnitList { get; set; }//TODO make baracs class
        public SpellBook SpellList { get; set; }

        #endregion

        #region Methods

        private void InitFields()
        {
            Attributes = new PlayerAttributes();
            //temporary-------------
            Attributes.SpellsLimit = 6;
            Attributes.UnitsLimit = 6;
            //----------------------
            UnitList = new List<UnitBase>();
            SpellList = new SpellBook(this);
        }

        public void Cast(ICastable fSpell, List<ThingBase> targets)
        {
            
        }

        public bool IsOwnedByPlayer(ThingBase something)//TODO other types, when implementation of SpellBook and Baracks is done.
        {
            if (something.GetType() == typeof(Player))
            {
                var player = (Player)something;
                return player == this;
            }
            if (something.GetType() == typeof(UnitBase))
            {
                var unit = (UnitBase)something;

                //var result = (from u in UnitList


                //baracks.contains unit;
                return true;
            }

            return false;
        }

        #endregion


    }
}
