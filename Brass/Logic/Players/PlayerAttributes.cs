﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brass.Logic.Players
{
    public class PlayerAttributes
    {
        public short Health { get; set; }
        public short Mana { get; set; }
        public short Gold { get; set; }

        public short MightModifier { get; set; }
        public short MagicModifier { get; set; }
        public short ResistanceModifier { get; set; }
        public short QicknessModifier { get; set; }

        public short SpellsLimit { get; set; }
        public short UnitsLimit { get; set; }
        
    }
}
