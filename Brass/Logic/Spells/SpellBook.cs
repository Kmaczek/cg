﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using Brass.Logic.Players;

namespace Brass.Logic.Spells
{
    public delegate void ExeededSpellLimitHandler(object sender, EventArgs ea);

    public class SpellBook
    {
            #region Private Fields

        private List<ICastable> _spellList;
        private Player _owner;

            #endregion

            #region Properties

        short SpellLimit { get; set; }

            #endregion

            #region Events

        public event ExeededSpellLimitHandler ExeededSpellLimit;

            #endregion

            #region Invokers

        protected virtual void OnExeededSpellLimit(EventArgs e)
        {
            var handler = ExeededSpellLimit;
            if (handler != null) handler(this, e);
        }

            #endregion

            #region Constructors

        public SpellBook(Player owner)
        {
            _owner = owner;
            _spellList = new List<ICastable>();
            SpellLimit = _owner.Attributes.SpellsLimit;
        }

        public SpellBook(Player owner, IEnumerable<ICastable> list)
        {
            _owner = owner;
            _spellList = new List<ICastable>(list);
            SpellLimit = _owner.Attributes.SpellsLimit;
        }

            #endregion

            #region Methods

        public bool Add(ICastable spell)
        {
            if (SpellLimit < (_spellList.Count + 1))
            {
                //OnExeededSpellLimit(EventArgs.Empty);
                return false;
            }

            _spellList.Add(spell);

            return true;
        }

        public bool ContainsReferenceOf(SpellBase spell)
        {
            return _spellList.Any(s => s == spell);
        }

        public ICastable GetSpell(short id)
        {
            return (from castable in _spellList 
                    let spell = (SpellBase)castable 
                    where spell.Id == id 
                    select castable).FirstOrDefault();
        }

        public ICastable GetFirsSpell()
        {
            return _spellList.FirstOrDefault();
        }

            #endregion
        
    }
}
