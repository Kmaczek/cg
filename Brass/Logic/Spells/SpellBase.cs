﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Brass.Logic.Constants;
using Brass.Logic.General;
using Brass.Logic.Helpers;
using Brass.Logic.Players;

namespace Brass.Logic.Spells
{
    /// <summary>
    /// Base of all spells, containing basic fields and functionality
    /// 
    /// </summary>
    public abstract class SpellBase : ThingBase //TODO Change to abstract when done
    {//TODO make schema for SpellBase
            #region Constructors

        protected SpellBase(Player owner, Player oponent, short id)
            : base("", ThingType.Spell)
        {
            SetPlayers(owner, oponent);
            InitFields();
            Id = id;
        }

            #endregion

            #region Methods

        /// <summary>
        /// Initialization of all class fields
        /// </summary>
        private void InitFields()
        {
            Id = 0;
            Attributes = new SpellAttributes();
            SchoolPrimary = School.None;
            States = SpellState.None;
            Targets = new List<Target>();
        }

        /// <summary>
        /// Seting players, do it before using Cast and other function related with it.
        /// </summary>
        /// <param name="owner">Player that is casting this spell</param>
        /// <param name="oponent">Oposite player</param>
        protected void SetPlayers(Player owner, Player oponent)
        {
            Owner = owner;
            Oponent = oponent;
        }

        /// <summary>
        /// Use it to load all data from XML.
        /// </summary>
        protected virtual void ParseData()
        {
            var stuff = XElement.Parse(Properties.Resources.SpellTesting);
            var result = GetSpellElement(stuff);
            ParseBasic(result);
        }

        /// <summary>
        /// Loading data from XML file located in resources. Id Property need to be set.
        /// To change file, change resource in private field _xElement
        /// </summary>
        /// <exception cref="ArgumentNullException">Casted when no data has been read from xml file</exception>
        /// <exception cref="ArgumentException">Casted while Id is not set</exception>
        protected void ParseBasic(XElement result)
        {
            if (result == null)
            {
                throw new ArgumentNullException("Be sure that field 'element' " +
                                                "is set properly");
            }

            Name = (string)result.Element(XSpellBase.Name);

            var help = new XmlHelper();
            Attributes = help.ParseSpellAtributes(result.Element(XSpellBase.Attributes));
            States = help.ParseSpellState(result.Element(XSpellBase.States));
            Targets = help.ParseTarget(result.Element(XSpellBase.Targets));
            SchoolPrimary = help.ParseSchool(result.Element(XSpellBase.SchoolPrimary));
            SchoolSecondary = help.ParseSchool(result.Element(XSpellBase.SchoolSecondary));
            SpellTypes = help.ParseSpellType(result.Element(XSpellBase.SpellTypes));
        }

        /// <summary>
        /// Selects Spell from xml file. Checks if Id is set, if not
        /// exeption is thrown
        /// </summary>
        /// <param name="element">Root directory in xml that contains spells(Spell) elements</param>
        /// <returns>XElement of spell id</returns>
        protected XElement GetSpellElement(XElement element)
        {
            if (Id < 1)
            {
                throw new ArgumentException("Incorrect value of Id: " + Id);
            }

            var stuff = element.Elements().ToList();
            var result = (from s in stuff
                          where (short)s.Element(XSpellBase.Id) == Id
                          select s).FirstOrDefault();

            return result;
        }

        //public bool IsTargetProper(ThingBase target)
        //{
        //    if (target == null) return false;
        //    if (target.ClassType == ThingType.Player)
        //    {
        //        //if(Targets.)
        //    }
        //}

            #endregion

            #region Properties

        public short Id { get; private set; }
        public SpellAttributes Attributes { get; set; }
        public School SchoolPrimary { get; private set; }
        public School SchoolSecondary { get; private set; }
        public SpellState States { get; set; }
        public List<Target> Targets { get; set; }
        public SpellType SpellTypes { get; private set; }

            #endregion

            #region Protected Fields

        protected Player Owner;
        protected Player Oponent;

            #endregion

            #region Private Fields

        

            #endregion
    }



    /// <summary>
    /// Class that is helpfull when parsing xml
    /// </summary>
    public static class XSpellBase
    {
        public static String Id = "Id";
        public static String Attributes = "Attributes";
        public static String SchoolPrimary = "SchoolPrimary";
        public static String SchoolSecondary = "SchoolSecondary";
        public static String States = "States"; 
        public static String Targets = "Targets";
        public static String SpellTypes = "SpellTypes";

        public static String Name = "Name";
        public static String Type = "Type";

        public static string ManaCost = "ManaCost";
        public static string GoldCost = "GoldCost";
        public static string Cooldown = "Cooldown";
        public static string BuyPrice = "BuyPrice";
        public static string SellPrice = "SellPrice";
    }
}
