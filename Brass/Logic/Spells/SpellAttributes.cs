﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brass.Logic.Spells
{
    [DebuggerDisplay("ManaCost={ManaCost}, GoldCost={GoldCost}, Cooldown={Cooldown}, " +
                     "BuyPrice={BuyPrice}, SellPrice={SellPrice}")]
    public class SpellAttributes
    {
        //public SpellAttributes()
        //{
        //    SetFields();
        //}

            #region Properties

        public short ManaCost { get; set; }
        public short ManaCostCurrent { get; set; }

        public short GoldCost { get; set; }
        public short GoldCostCurrent { get; set; }

        public short Cooldown { get; set; }
        public short CooldownCurrent { get; set; }

        public short BuyPrice { get; set; }
        public short BuyPriceCurrent { get; set; }

        public short SellPrice { get; set; }
        public short SellPriceCurrent { get; set; }

            #endregion

            #region Methods

        /// <summary>
        /// Returns string with current state of attributes
        /// ex. Spell Atributes => Mana: 30, 30 | Gold: 0, 0 | Cooldown: 1, 1 | Buy: 560, 560 | Sell: 180, 180 | 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Spell Atributes => ");
            sb.Append("Mana: ");
            sb.Append(ManaCost+", "+ManaCostCurrent+ " | ");
            sb.Append("Gold: ");
            sb.Append(GoldCost + ", " + GoldCostCurrent + " | ");
            sb.Append("Cooldown: ");
            sb.Append(Cooldown + ", " + CooldownCurrent + " | ");
            sb.Append("Buy: ");
            sb.Append(BuyPrice + ", " + BuyPriceCurrent + " | ");
            sb.Append("Sell: ");
            sb.Append(SellPrice + ", " + SellPriceCurrent + " | ");

            return sb.ToString();
        }

        /// <summary>
        /// Initializing Fields
        /// </summary>
        private void SetFields()
        {
            ManaCost = 0;
            ManaCostCurrent = 0;
            GoldCost = 0;
            GoldCostCurrent = 0;
            BuyPrice = 0;
            BuyPriceCurrent = 0;
            SellPrice = 0;
            SellPriceCurrent = 0;
            Cooldown = 0;
            CooldownCurrent = 0;
        }

            #endregion

    }

}
    