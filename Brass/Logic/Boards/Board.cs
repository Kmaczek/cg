﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using Brass.Logic.General;
using Brass.Logic.Players;
using Brass.Logic.Spells;
using Brass.Logic.Units;

namespace Brass.Logic.Boards
{
    class Board
    {
        private Player _playerOne;
        private Player _playerTwo;

        private List<ICastable> Spells; //SpellShop
        private List<UnitBase> Units; //Baracks

        public Board()
        {
            //creating players
            _playerOne = new Player("Mieszko");
            _playerTwo = new Player("Brynjolf");
            //creating spells
            var spell = new HealthSpell(_playerOne, _playerTwo, 1);
            var spell2 = new HealthSpell(_playerTwo, _playerOne, 2);
            //creating units
            var unit = new UnitBase(1);
            //testing spells
            Console.WriteLine("HP: {0}", unit.Attributes.Health);
            _playerOne.SpellList.Add(spell);
            //setting targets
            var targets = new List<ThingBase>();
            targets.Add(unit);
            //casting spell on unit
            _playerOne.SpellList.GetFirsSpell().Cast(targets);
            Console.WriteLine("HP: {0}", unit.Attributes.Health);

            //same action with casting on unit and player
            _playerTwo.SpellList.Add(spell2);
            targets.Add(_playerTwo);
            Console.WriteLine("Player2 health: {0}", _playerTwo.Attributes.Health);
            _playerTwo.SpellList.GetFirsSpell().Cast(targets);
            Console.WriteLine("Player2 health: {0}", _playerTwo.Attributes.Health);
            Console.WriteLine("HP: {0}", unit.Attributes.Health);

            var spelA = new HealthSpell(_playerOne, _playerTwo, 1);
            var spelB = new HealthSpell(_playerOne, _playerTwo, 1);
            var spelC = spelA;

            Console.WriteLine("Compared: {0}", spelA == spelC);

        }
    }
}
