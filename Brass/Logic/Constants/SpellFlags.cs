﻿using System;
using System.Diagnostics;

namespace Brass.Logic.Constants
{
    //Use System.Enum to manipulate flags

    /// <summary>
    /// Flags that describe spell state
    /// </summary>
    //[DebuggerDisplay]
    [Flags]
    public enum SpellState : short
    {
        None = 0,
        Dispellable = 1 << 0,
        Positive = 1 << 1,
        InterruptImmune = 1 << 2,
        //CantAttack = 0x8,
        //Clumsy = 0x10
    }

    [Flags]
    public enum SpellType : short
    {
        None = 0x0,
        DirectDamage = 1 << 0,
        DirectHealing = 1 << 1,
        DamageOverTurn = 1 << 2,
        Summoning = 1 << 3,
        Displacement = 1 << 4,
        Debuff = 1 << 5,
        Buff = 1 << 6,
        Control = 1 << 7
    }
}