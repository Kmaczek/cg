﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brass.Logic.Constants
{
    public struct Target
    {
        public TargetFlag Attitude;
        public ThingType Affiliation;
    }

    public struct XTarget
    {
        public static String Attitude = "Attitude";
        public static String Affiliation = "Affiliation";
    }
}
