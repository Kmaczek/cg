﻿using System;

namespace Brass.Logic.Constants
{
    //Use System.Enum to manipulate flags

    /// <summary>
    /// Flags for targeting spells, abilities and normal atacks
    /// </summary>
    [Flags]
    public enum TargetFlag : short
    {
        None = 0,
        Owned = 1 << 1,
        Opposing = 1 << 2,

        Any = Opposing | Owned,
    }


}