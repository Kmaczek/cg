﻿using System;

namespace Brass.Logic.Constants
{
    /// <summary>
    /// School of Unit, Spell, sometimes Item, Ability, Player. It is used
    /// to determine various dependencis between Cards
    /// </summary>
    public enum School : byte
    {
        None = 0,
        WichCraft,
        Sorcery,
        Survival,
        ArtOfWar,
        Devotion
    }
        
    /// <summary>
    /// xml helper class
    /// </summary>
    public static class XSchool
    {
        public static String None = "None";
        public static String WichCraft = "WichCraft";
        public static String Sorcery = "Sorcery";
        public static String Survival = "Survival";
        public static String ArtOfWar = "ArtOfWar";
        public static String Devotion = "Devotion"; 

    }
}