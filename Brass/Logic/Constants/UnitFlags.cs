﻿using System;

namespace Brass.Logic.Constants
{
    //Use System.Enum to manipulate flags

    /// <summary>
    /// Flags usefull to determine Unit state
    /// </summary>
    [Flags]
    public enum UnitState : short
    {
        None = 0,
        Burn = 1 << 0,
        Frozen = 1 << 1,
        Bleeding = 1 << 2,
        Cursed = 1 << 3
    }

    /// <summary>
    /// Flags usefull to determine Unit state
    /// </summary>
    [Flags]
    public enum UnitType : short
    {
        None = 0,
        Living = 1 << 0,
        Mechanic = 1 << 1,
        Undead = 1 << 2,
        Ranged = 1 << 3,
        Mele = 1 << 4

    }
}