﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Brass.Logic.Spells;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Brass.Logic.Helpers;

namespace Brass.Logic.Helpers.Test
{
    [TestClass()]
    public class XmlHelperTests
    {
        [TestMethod()]
        public void ParseSpellAtributesTest()
        {
            XElement xElement = XElement.Parse(BrassTests.Resourc.spellParse);

            var stuff = xElement.Elements().ToList();
            var result = (from s in stuff
                          where (short)s.Element(XSpellBase.Id) == 1
                          select s).FirstOrDefault();

            var help = new Helpers.XmlHelper();

            var sa = help.ParseSpellAtributes(result.Element(XSpellBase.Attributes));
            Assert.AreEqual(sa.BuyPrice, 560);
            Assert.AreEqual(sa.BuyPriceCurrent, 560);
            Assert.AreEqual(sa.Cooldown, 1);
            Assert.AreEqual(sa.CooldownCurrent, 1);
            Assert.AreEqual(sa.GoldCost, 0);
            Assert.AreEqual(sa.GoldCostCurrent, 0);
            Assert.AreEqual(sa.ManaCost, 30);
            Assert.AreEqual(sa.ManaCostCurrent, 30);
            Assert.AreEqual(sa.SellPrice, 180);
            Assert.AreEqual(sa.SellPriceCurrent, 180);
        }
    }
}
